import paho.mqtt.client as mqtt
import json
import time
from random import *
import datetime

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("connected OK")
    else:
        print("Bad connection Returned code=", rc)

def on_disconnect(client, userdata, flags, rc=0):
    print(str(rc))

def on_publish(client, userdata, mid):
    print("In on_pub callback mid= ", mid)


# 새로운 클라이언트 생성
client = mqtt.Client()
# 콜백 함수 설정 on_connect(브로커에 접속), on_disconnect(브로커에 접속중료), on_publish(메세지 발행)
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_publish = on_publish
# address : localhost, port: 1883 에 연결
client.connect('localhost', 1883)
client.loop_start()
# common topic 으로 메세지 발행 # UI로 보낼때

col = ["time", "vehState", "vehId", "location", "loss", "gyro", "driveLeft", "driveRight", "pusher"]
ranges = [[None, None], [1, 10], [13,51],[1, 100],[0,0.008],[-500, 300], [-0.0008, 0.0008]] # speed, torque, acc ///// loss, lf
col2 = [["accX", "accY", "accZ", "yaw", "accXLF", "accYLF", "accZLF", "yawLF"], ["speed", "torque", "speedLF", "torqueLF"]] # 5, 678

def range_float(range_value) :
    return uniform(range_value[0], range_value[1])
def range_int(range_value) :
    return randrange(range_value[0], range_value[1])

def make_row() :
    row = {}
    for i, col_name in enumerate(col) :
        if col_name == 'time' :
            row[col_name] = int(time.time()*1000) # ms
        elif col_name == 'loss' :
            row[col_name] = range_float(ranges[i])
        elif col_name == "gyro":
            tmp = {}
            for col_name2 in col2[0] :
                tmp[col_name2] = range_float(ranges[6])
            row[col_name] = tmp
        elif col_name in ["driveLeft", "driveRight", "pusher"] :
            tmp = {}
            for idx, col_name2 in enumerate(col2[1]) :
                if idx < 2 :
                    tmp[col_name2] = range_float(ranges[5])
                else :
                    tmp[col_name2] = range_float(ranges[6])
            row[col_name] = tmp
        elif col_name == 'vehId' :
            row[col_name] = 'AGV_1'
        elif col_name == 'location' :
            row[col_name] = 'P1234'
        else :
            row[col_name] = range_int(ranges[i])
    return row


idx = 0

sentTime = time.time()
while True :
    curTime = time.time()
    if (curTime - sentTime > 0.01):
        data = make_row()
        print(json.dumps(data, indent=4))
        client.publish('testing', json.dumps(data), 1)
        sentTime = time.time()


client.loop_stop()
# 연결 종료
client.disconnect()